<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

Route::group(['prefix' => 'love-story'], function () {
	Route::get('/', function () {
	    return view('index');
	});
    Route::get('1', function () {
	    return view('Love.1.index');
	});
	Route::get('2', function () {
	    return view('Love.2.index');
	});
	Route::get('3', function () {
	    return view('Love.3.index');
	});
	Route::get('4', function () {
	    return view('Love.4.index');
	});
	Route::get('5', function () {
	    return view('Love.5.index');
	});
	Route::get('6', function () {
	    return view('Love.6.index');
	});
	Route::get('7', function () {
	    return view('Love.7.index');
	});
	Route::group(['prefix' => '8'], function () {
		Route::get('/', function () {
		    return view('Love.8.index1');
		});
		Route::get('1', function () {
		    return view('Love.8.index1');
		});
		Route::get('2', function () {
		    return view('Love.8.index2');
		});
		Route::get('3', function () {
		    return view('Love.8.index3');
		});
	});
});