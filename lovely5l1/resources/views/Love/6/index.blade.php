<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>My Lonely Life</title>
	
	<style type="text/css">
		@font-face {
			font-family: digit;
			src: url({{asset('/Love/6/digital-7_mono.ttf')}}) format("truetype");
		}
	</style>
	<link href="{{asset('/Love/6/jscss/default.css')}}" type="text/css" rel="stylesheet">
	<script type="text/javascript" src="{{asset('/Love/6/jscss/jquery.js')}}"></script>
	<script type="text/javascript" src="{{asset('/Love/6/jscss/garden.js')}}"></script>
</head>

<body>
	<div id="mainDiv">
		<div id="content" style="width: 1160px; height: 625px; margin-top: 52px; margin-left: 220px;">
			<div id="code" style="margin-top: 62.5px;">
			
				Boy i = <span class="keyword">new</span> Boy(<span class="string">"De Meng"</span>);<br>
				<span class="comments">// It's De Meng. </span><br>
				Girl u ;<br>
				<span class="keyword">while</span> (true) {<br>
				<span class="placeholder"><span class="keyword">try</span> {<br>
				<span class="placeholder"><span class="placeholder">u = <span class="keyword">new</span> Girl(<span class="string">"Qing Li"</span>); <br>
					<span class="placeholder">}<br>
				<span class="placeholder"><span class="keyword">catch</span> (Exception e) {<br>
					<span class="placeholder"><span class="placeholder"><span class="keyword">continue</span>; <br>
						<span class="placeholder">	}<br>
				<span class="placeholder">if (u != <span class="string">NULL</span>){ break; }</span><br> 
				}<br>
					<span class="keyword">try</span>{<br>
					<span class="placeholder"><span class="keyword">while</span> (<span class="keyword">true</span>) {<br>
					<span class="placeholder"><span class="placeholder"><span class="keyword">switch</span>( u-&gt;status() ){<br>
					<span class="placeholder"><span class="placeholder"><span class="keyword">case</span><span class="string">"study abroad TW"</span>: i-&gt;wait(u);break; <br>
					<span class="placeholder"><span class="placeholder"><span class="keyword">case</span><span class="string">"come back VN"</span>: i-&gt;marry(u);break; <br>
					<span class="placeholder"><span class="placeholder"><span class="keyword">case</span><span class="string">"sad"</span>: i-&gt;hug(u);break; <br>
					<span class="placeholder"><span class="placeholder"><span class="keyword">case</span><span class="string">"sleepy"</span>: i-&gt;hug(u);break; <br>
					<span class="placeholder"><span class="placeholder"><span class="keyword">default</span>: i-&gt;love(u);break; <br>
					<span class="placeholder"><span class="placeholder">}<br>
					<span class="placeholder">}<br>
				}<br>
				<span class="keyword">catch</span>(u.DoNotLoveMe(i)){ <br>
				<span class="placeholder">system.all().detroy(true); <span class="comments"><br>// Detroy every thing!.</span><br>
				}<br>
			</span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></div>
			<div id="loveHeart">
				<canvas id="garden" width="670" height="625"></canvas>
				<div id="words">
					<div id="messages">
						&nbsp;&nbsp;QL, De Meng have &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<br> waiting for you<br> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<br>
						<div id="elapseClock">&nbsp;&nbsp;&nbsp;&nbsp;<span class="digit">333</span> days <span class="digit">17</span> hrs <span class="digit"><br>&nbsp;&nbsp;07</span> mins <span class="digit">33</span> secs</div>
					</div>
					
					<div id="loveu">
						You are my life.<br>
						<div class="signature">- De Meng</div>
					</div>
				</div>
			</div>
		</div>
		
	</div>
	<script type="text/javascript">
		var offsetX = $("#loveHeart").width() / 2;
		var offsetY = $("#loveHeart").height() / 2 - 55;
		var nolovetime = new Date();
		nolovetime.setFullYear(2018, 12, 30);
		nolovetime.setHours(5);
		nolovetime.setMinutes(0);
		nolovetime.setSeconds(0);
		nolovetime.setMilliseconds(0);
		
		if (!document.createElement('canvas').getContext) {
			var msg = document.createElement("div");
			msg.id = "errorMsg";
			msg.innerHTML = "Your browser doesn't support HTML5!<br/>Recommend use Chrome 14+/IE 9+/Firefox 7+/Safari 4+"; 
			document.body.appendChild(msg);
			$("#code").css("display", "none")
			$("#copyright").css("position", "absolute");
			$("#copyright").css("bottom", "10px");
		    document.execCommand("stop");
		} else {
			setTimeout(function () {
				startHeartAnimation();
			}, 100);

			timeElapse(nolovetime);
			setInterval(function () {
				timeElapse(nolovetime);
			}, 500);

			adjustCodePosition();
			$("#code").typewriter();
		}
	</script>


</body></html>